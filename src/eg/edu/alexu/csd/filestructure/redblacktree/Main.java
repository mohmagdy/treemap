package eg.edu.alexu.csd.filestructure.redblacktree;

public class Main {
    public static void main(String[] args) {
        RedBlackTree<Integer,String> tree = new RedBlackTree<>();
        tree.insert(0,"a");
        tree.insert(1,"a");
        tree.insert(2,"a");
        tree.insert(3,"a");
        tree.insert(4,"a");
        tree.insert(5,"a");
        tree.insert(6,"a");
        tree.delete(0);
        tree.print(tree.getRoot());
        System.out.println();
        tree.delete(1);
        tree.print(tree.getRoot());
        System.out.println();
        tree.delete(2);
        tree.print(tree.getRoot());
        System.out.println();
        tree.delete(3);
        tree.print(tree.getRoot());
        System.out.println();
        tree.delete(4);
        tree.print(tree.getRoot());
        System.out.println();

    }

}


